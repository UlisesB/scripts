#! /usr/bin/bash

Dir=$(dirname ${0})
Instances=$(cat ${Dir}/instances.txt)
Output=${Dir}/output.txt ### Choose the output here ###

echo '' > ${Dir}/rank.txt

for Instance in ${Instances}; do
    ping -c 1 -w 1 ${Instance} > ./ping.txt 2>&1 # Pings the instance once.
    ExitCode=${?}
#    echo "${Instance} exit code: ${ExitCode}"
    Speed=$(cat ./ping.txt | head -2 | tail -1 | cut -f8 -d ' ' | cut -b 6-) # Gets the time from the huge ping output.
    if [ ${ExitCode} = 0 ]; then # If ping doesn't fail,
#        echo ${Speed} ${Instance}
        echo ${Speed} ${Instance} >> ${Dir}/rank.txt # save the speed and instance.
    fi
done

sort ${Dir}/rank.txt | head -2 | tail -1 | cut -f2 -d ' ' > ${Output} # Sort by speed and write to the output.
