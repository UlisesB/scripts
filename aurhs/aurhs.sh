#! /usr/bin/bash

cd ${HOME}

if [ ${1} = -I ]; then
    Packs=$(echo ${@} | cut -b 3-)
    for Pack in ${Packs}; do
        mkdir ${HOME}/Builds/${Pack}
        git -C ${HOME}/Builds/${Pack} clone https://aur.archlinux.org/${Pack}.git
        cat ${HOME}/Builds/${Pack}/${Pack}/PKGBUILD
        echo 'Is this PKGBUILD OK? (y/n)'
        read Answer
        if [ ${Answer} = y -o ${Answer} = Y -o ${Answer} = yes -o ${Answer} = Yes ]; then
            cd ${HOME}/Builds/${Pack}/${Pack}
            makepkg -s -r -c
            cd ${HOME}
            InstallPacks=${InstallPacks} ${Pack}
        fi
    done
    for InstallPack in ${InstallPacks}; do
        sudo pacman -U ${HOME}/Builds/${InstallPack}/${InstallPack}-*.pkg.tar.zst
    done
elif [ ${1} = -U ]; then
    Packs=$(echo ${@} | cut -b 3-)
    for Pack in ${Packs}; do
        git -C ${HOME}/Builds/${Pack} pull
        cat ${HOME}/Builds/${Pack}/${Pack}/PKGBUILD
        echo 'Is this PKGBUILD OK? (y/n)'
        read Answer
        if [ ${Answer} = y -o ${Answer} = Y -o ${Answer} = yes -o ${Answer} = Yes ]; then
            cd ${HOME}/Builds/${Pack}/${Pack}
            makepkg -s -r -c
            cd ${HOME}
            UpdatePacks=${UpdatePacks} ${Pack}
        fi
    done
    for UpdatePack in ${UpdatePacks}; do
        sudo pacman -U ${HOME}/Builds/${UpdatePack}/${UpdatePack}-*.pkg.tar.zst
    done
else
    echo 'Bad arguments. Exiting'
    exit 1
fi
