#!/bin/bash

clear

# Directory of the script
dir=$(${HOME}/scripts/yt-u)

# Configuration
configPath=${HOME}/.config/yt-u
cache=${dir}/cache.txt



# RE gets the last 15 videos (limit set by YouTube) from the channels specified
# in ${configPath}/subscriptions.txt through their RSS feed. It greps the lines
# with important data, strips non-human-friendly bits, replaces some spaces with
# ç so they're easier to deal with and then groups the output so each line
# corresponds to 1 video. Finally it sorts them by date, numbers them and puts
# the channel name first, then the video title and then the video ID
RE() {
CL
channelCount=$(wc -l ${configPath}/subscriptions.txt | awk '{print $1}')
echo -n '' > ${cache}
completedChannels=0
echo -ne "Reloading the subscription feed... 0/${channelCount}\r"
for channelId in $(awk '{print $1}' ${configPath}/subscriptions.txt); do
    lineNum=0
    for line in $(curl -s "https://www.youtube.com/feeds/videos.xml?channel_id=${channelId}" \
    | grep "<yt:videoId>\|<name>\|<media:title>\|<published>" \
    | sed -E -e 's/.*<yt:videoId>//' -e 's/<\/yt:videoId>//' -e 's/.*<name>//' \
    -e 's/<\/name>//' -e 's/.*<media:title>//' -e 's/<\/media:title>//' \
    -e 's/.*<published>//' -e 's/<\/published>//' -e 's/&quot;/"/g' -e 's/&amp;/\&/g' \
    -e 1,2d -e 's/ /ç/g'); do
        lineNum=$((${lineNum}+1))
        [ ${lineNum} -ne 4 ] && echo -n "${line} " >> ${cache}
        [ ${lineNum} -eq 4 ] && echo ${line} >> ${cache} && lineNum=0
    done
    completedChannels=$((${completedChannels}+1))
    echo -ne "Reloading the subscription feed... ${completedChannels}/${channelCount}\r"
done

awk '{print $3, $2, $4, $1}' ${cache} | sort -r | awk '{print $2, $3, $4}' \
| nl -w1 -s\  | tac > ${dir}/SU.txt
CL
}



# PA draws pages. It doesn't output the last column so ugly data (like video IDs) can be
# stored there, puts everything into nice columns, replaces the çs with actual spaces and
# sets the current displayed page
PA() {
clear
awk '{$NF=""; print $0}' ${dir}/${1}.txt \
| column -t -o ' ' | sed 's/ç/ /g'
currentPage=${1}
}



#HI() {

#}



#SE() {

#}



#IN() {

#}



# DO calls PA if no arguments are given, or downloads the specified videos with yt-dlp
DO() {
[ -z ${@} ] && PA DO
[ -n ${@} ] &&
for videoNum in ${@}; do
    videoId=$(tail -${videoNum} ${dir}/${currentPage}.txt \
    | head -1 | awk '{print $NF}')

    yt-dlp --quiet --no-download-archive --dateafter 11111111 \
    --output "${dir}/downloads/%(channel)s %(title)s .%(ext)s" \
    "https://www.youtube.com/watch?v=${videoId}" &
done
}



# WA opens videos with mpv and yt-dlp
WA() {
videoId=$(tail -${1} ${dir}/${currentPage}.txt \
| head -1 | awk '{print $NF}')

nohup mpv --save-position-on-quit=yes --ytdl=yes \
--ytdl-raw-options=no-abort-on-error=,ignore-config=,geo-bypass=,no-match-filter=,no-playlist=,no-download-archive=,concurrent-fragments=1,limit-rate=6M,retries=2,fragment-retries=4,skip-unavailable-fragments=,no-keep-fragments=,buffer-size=2048,resize-buffer=,no-playlist-reverse=,no-hls-use-mpegts=,no-batch-file=,paths=temp,output="${videoId}.%(ext)s",output-na-placeholder='yt-u_video',no-restrict-filenames=,no-windows-filenames=,force-overwrites=,no-continue=,no-part=,mtime=,no-write-description=,no-write-info-json=,no-write-playlist-metafiles=,clean-infojson=,no-write-comments=,no-cookies=,no-cookies-from-browser=,no-cache-dir=,rm-cache-dir=,no-write-thumbnail=,no-ignore-no-formats-error=,no-progress=,format='bv[height<=1080][fps<=60]+ba/b[height<=1080][fps<=60]',no-video-multistreams=,no-audio-multistreams=,prefer-free-formats=,no-check-formats=,write-subs=,no-write-auto-subs=,sub-langs=en.*,audio-quality=0,no-keep-video=,post-overwrites=,no-embed-subs=,no-embed-thumbnail=,no-embed-metadata=,no-embed-chapters=,fixup=detect_or_warn,no-split-chapters=,no-remove-chapters=,no-force-keyframes-at-cuts=,sponsorblock-remove=sponsor,extractor-retries=2,allow-dynamic-mpd=,no-hls-split-discontinuity= \
"https://www.youtube.com/watch?v=${videoId}" &
CL
}



# CL clears the prompt by just redrawing the current page
CL() {
PA ${currentPage}
}



EX() {
clear
exit 0
}



DEBUG() {
clear
echo $(tail -4 ${dir}/${currentPage}.txt \
| head -1 | awk '{print $NF}')
currentPage=DEBUG
}



INPUT() {
echo -n '>> '
read answer
[ -z "${answer}" ] && CL && INPUT
[ "${answer}" = he ] && PA HE && INPUT
[ "${answer}" = re ] && RE && INPUT
[ "${answer}" = su ] && PA SU && INPUT
[ "${answer}" = hi ] && HI && INPUT
[ $(echo "${answer}" | awk '{print $1}') = wa ] && WA $(echo "${answer}" | awk '{print $2}') && INPUT
[ $(echo "${answer}" | awk '{print $1}') = do ] && DO $(echo "${answer}" | awk '{print $2}') && INPUT
[ "${answer}" = cl ] && CL && INPUT
[ "${answer}" = ex ] && EX && INPUT
[ "${answer}" = de ] && DEBUG && INPUT
CL
echo -n 'Command not recognized. '
INPUT
}



PA SU

INPUT
