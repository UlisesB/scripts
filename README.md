# Scripts

My scripts.

# AUR Helper Script

Very simple "AUR Helper" to "automatically" install and update AUR packages.

# Invidious Ranker

Simple script to rank specified invidious instances by speed.

Set instances to sort in instances.txt and the output in the $Output variable in the script, by default it's output.txt in the script directory.
instances.txt must be in the same folder as invidious-ranker.sh.
